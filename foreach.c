#include <stdlib.h>

#include "struct.h"

void linkedlist_foreach(struct linkedlist* l, void (*callback)(void*))
{
	for(struct llnode* current = l->first;current;current = current->next)
	{
		callback(current->data);
	}
}


#ifndef LIBLINKEDLIST_H
#define LIBLINKEDLIST_H
struct llnode
{
	struct llnode* prev, *next;
	void* data;
};

struct linkedlist
{
	struct llnode* first, *last;
	unsigned long n;
};
struct linkedlist new_linkedlist();
struct linkedlist linkedlist_clone(struct linkedlist* this, void* (*callback)(void*));
void linkedlist_push_n(struct linkedlist* this, void* ptr);
void linkedlist_pop_0(struct linkedlist* this);
void linkedlist_pop_n(struct linkedlist* this);
void linkedlist_foreach(struct linkedlist* this, void (*callback)(void*));
void delete_linkedlist(struct linkedlist* this);
#endif

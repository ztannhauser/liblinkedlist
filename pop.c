#include <assert.h>
#include <stdlib.h>

#include "struct.h"

void linkedlist_pop(
	struct linkedlist* this,
	struct llnode* removeme)
{
	if(this->first == removeme)
	{
		this->first = removeme->next;
	}
	else if(this->last == removeme)
	{
		this->last = removeme->prev;
	}
	else
	{
		struct llnode* prev = removeme->prev, *next = removeme->next;
		prev->next = next, next->prev = prev;
	}
	free(removeme);
	this->n--;
}

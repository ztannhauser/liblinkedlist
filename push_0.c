#include <stdlib.h>
#include <stdio.h>

#include "struct.h"

void linkedlist_push_0(struct linkedlist* this, void* ptr)
{
	struct llnode* new = malloc(sizeof(struct llnode));
	new->prev = NULL;
	new->next = this->first;
	new->data = ptr;
	if(this->first)
	{
		this->first->prev = new;
	}
	this->first = new;
	if(!this->last)
	{
		this->last = new;
	}
	this->n++;
}

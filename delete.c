#include <stdlib.h>

#include "struct.h"
#include "clear.h"

void delete_linkedlist(struct linkedlist* this)
{
	linkedlist_clear(this);
}

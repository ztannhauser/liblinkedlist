#include <stdlib.h>
#include <stdio.h>
#include <assert.h>

#include "struct.h"

void linkedlist_pop_n(struct linkedlist* this)
{
	struct llnode* oldlast = this->last;
	if(oldlast)
	{
		this->last = oldlast->prev;
		if(this->last)
		{
			this->last->next = NULL;
		}
		if(this->first == oldlast)
		{
			this->first = NULL;
		}
		free(oldlast);
		this->n--;
	}
}

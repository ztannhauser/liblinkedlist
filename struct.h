struct llnode
{
	struct llnode* prev, *next;
	void* data;
};

struct linkedlist
{
	struct llnode* first, *last;
	unsigned long n;
};

#include <assert.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>

#include "struct.h"

struct linkedlist linkedlist_clone(
	struct linkedlist* this,
	void* (*callback)(void*))
{
	struct linkedlist ret;
	ret.n = this->n;
	ret.first = NULL;
	struct llnode* prev = NULL;
	for(struct llnode* current = this->first;current;current = current->next)
	{
		struct llnode* new = malloc(sizeof(struct llnode));
		new->prev = prev;
		if(prev)
		{
			prev->next = new;
		}
		else
		{
			ret.first = new;
		}
		new->next = NULL;
		void* ptr = current->data;
		new->data = callback ? callback(ptr) : ptr;
		prev = new;
	}
	ret.last = prev;
	return ret;
}



CC = gcc
CPPFLAGS = -I .
TARGET = liblinkedlist

default: $(TARGET).a

srclist.mk:
	find -type f -regex '.*\.c' | sed 's/^/SRCS += /' > srclist.mk

include srclist.mk

OBJS = $(SRCS:.c=.o)
DEPENDS = $(SRCS:.c=.mk)

install: ../$(TARGET).a ../$(TARGET).h

../$(TARGET).a: $(TARGET).a
	cp $(TARGET).a ../

../$(TARGET).h: $(TARGET).h
	cp $(TARGET).h ../

target_h_deps += struct.h
target_h_deps += new.h
target_h_deps += push_n.h
target_h_deps += pop_n.h
target_h_deps += foreach.h
target_h_deps += delete.h

$(TARGET).h: $(target_h_deps)
	echo "#ifndef" $(TARGET)_h > $(TARGET).h
	echo "#define" $(TARGET)_h >> $(TARGET).h
	cat $(target_h_deps) >> $(TARGET).h
	echo "#endif" >> $(TARGET).h

$(TARGET).a: $(OBJS)
	$(LD) -r $^ -o $@

%.l.mk: %.l
	echo "$(basename $<).c: $<" > $@

%.y.mk: %.y
	echo "$(basename $<).c: $<" > $@

%.mk: %.c
	$(CPP) -MM -MT $@ $(CPPFLAGS) -MF $@ $<

%.o: %.c %.mk
	$(CC) -c $(CPPFLAGS) $(CFLAGS) $< -o $@

.PHONY: clean deep-clean

clean:
	rm $(OBJS) $(DEPENDS) srclist.mk main

deep-clean:
	find -type f -regex '.*\.mk' -delete
	find -type f -regex '.*\.o' -delete
	find -type f -executable -delete

include $(DEPENDS)


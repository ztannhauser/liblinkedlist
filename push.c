#include <assert.h>
#include <stdlib.h>

#include "struct.h"
#include "push_n.h"

void linkedlist_push(
	struct linkedlist* this,
	struct llnode* position,
	void* ptr)
{
	if(position)
	{
		struct llnode* new = malloc(sizeof(struct llnode));
		new->data = ptr;
		if(position == this->first)
		{
			this->first = new;
			new->prev = NULL;
		}
		else
		{
			(new->prev = position->prev)->next = new;
		}
		position->prev = new;
		new->next = position;
		this->n++;
	}
	else
	{
		linkedlist_push_n(this, ptr);
	}
}

#include <stdlib.h>

#include "struct.h"

void linkedlist_free_elements(struct linkedlist* this)
{
	for
	(
		struct llnode
			*current = this->first,
			*next = current ? current->next : NULL;
		current;
		current = next, next = current ? current->next : NULL
	)
	{
		free(current->data);
		free(current);
	}
	this->n = 0;
	this->first = this->last = NULL;
}

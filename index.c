#include <assert.h>
#include <stdlib.h>

#include "struct.h"

struct llnode* linkedlist_index(struct linkedlist* this, size_t index)
{
	struct llnode* current = this->first;
	while(current && index--)
	{
		current = current->next;
	}
	return current;
}

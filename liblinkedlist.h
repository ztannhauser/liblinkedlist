#ifndef liblinkedlist_h
#define liblinkedlist_h
struct llnode
{
	struct llnode* prev, *next;
	void* data;
};

struct linkedlist
{
	struct llnode* first, *last;
	unsigned long n;
};
struct linkedlist new_linkedlist();
void linkedlist_push_n(struct linkedlist* this, void* ptr);
void linkedlist_pop_n(struct linkedlist* this);
void linkedlist_foreach(struct linkedlist* this, void (*callback)(void*));
void delete_linkedlist(struct linkedlist* this);
#endif
